# xj_restful_webserver

## Below is the API with the method to call
```
'/tokenrefresh/'								                                GET
```
```
'/users/', '/users/<id_>'							                            GET
'/users/active'                        					                        GET
'/users/deactive'                      						                    GET
'/user/', '/user/<id_>'								                            PUT, DELETE
'/create/<type>/'                                                               POST
```
```
'/contents/', '/contents/<id_>'							                        GET	
'/contents/active/type/<id_>'				                                    GET
'/contents/active/author/<id_>'			                                        GET
'/contents/active/tags/<id_>'				                                    GET
'/contents/deactive/type/<id_>'			                                        GET
'/contents/deactive/author/<id_>'			                                    GET
'/contents/deactive/tags/<id_>'			                                        GET
'/content/', '/content/<id_>'							                        POST, PUT, DELETE
```
```
'/comments/', '/comments/<id_>'							                        GET
'/comments/active/content/<id_>'			                                    GET
'/comments/active/userid/<id_>'			                                        GET
'/comments/active/id/<id_>'				                                        GET
'/comments/deactive/content/<id_>'		                                        GET
'/comments/deactive/userid/<id_>'			                                    GET
'/comments/deactive/id/<id_>'				                                    GET
'/comment/', '/comment/<id_>'							                        POST, PUT, DELETE
```
```
'/login/'									                                    POST
```
```
'/sms_verify/'									                                POST
'/send_email/<id_>'									                            POST
'/active/<token>'									                            GET
```