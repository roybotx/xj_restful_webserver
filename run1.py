from flask import Flask, request, Response, current_app, make_response, send_file, g, jsonify, abort, redirect,url_for, send_from_directory
from flask_restful import Resource, Api,reqparse
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import Unauthorized
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
from sqlalchemy import create_engine,inspect
from functools import wraps
from json import dumps, load, dump
from tempfile import NamedTemporaryFile
from shutil import copyfileobj
import urllib.parse as urlparse
import os, subprocess, sys, signal
import flask_jwt
from flask_jwt import JWT, jwt_required, current_identity,JWTError
from werkzeug.security import safe_str_cmp
from datetime import datetime, timedelta
import jwt as pyjwt
import time
from base64 import b64encode
import requests
import random
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import smtplib
import glob
from threading import Lock
import shutil
import mimetypes


app = Flask(__name__)
dbConnectionString='sqlite:///seeking_ai.db'
rootpath="/api/v1.0"
app.config['SQLALCHEMY_DATABASE_URI'] = dbConnectionString
app.config['SECRET_KEY']='\xcafW-\xbeX\x98-\x1a\xb2g\xc1\x0e\x87\xb2\x83[\xa0\x0fi/\x18\x85\xda'
app.config['JWT_AUTH_URL_RULE'] = '/login' 
app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=3600 * 24 * 7)

class LockException(Exception):
    LOCK_FAILED = 1

database_lock = Lock()

# if os.name == 'nt':
#     import win32file, win32con, pywintypes
#     LOCK_EX = win32con.LOCKFILE_EXCLUSIVE_LOCK
#     LOCK_SH = 0
#     LOCK_NB = win32con.LOCKFILE_FAIL_IMMEDIATELY
    
#     __overlapped = pywintypes.OVERLAPPED()
#     def lock(file, flags):
#         hfile = win32file._get_osfhandle(file.fileno())
#         try:
#             win32file.LockFileEx(hfile, flags, 0, -0x10000, __overlapped)
#         except (pywintypes.error, exc_value) as e:
#             if exc_value[0] == 33:
#                 raise LockException(LockException.LOCK_FAILED, exc_value[2])
#             else:
#                 raise

#     def unlock(file):
#         hfile = win32file._get_osfhandle(file.fileno())
#         try:
#             win32file.UnlockFileEx(hfile, 0, -0x10000, __overlapped)
#         except (pywintypes.error, exc_value) as e:
#             if exc_value[0] == 158:
#                 pass
#             else:
#                 raise
# else:
#     raise RuntimeError

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    if request.method == 'OPTIONS':
        response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
    headers = request.headers.get('Access-Control-Request-Headers')
    if headers:
        response.headers['Access-Control-Allow-Headers'] = headers
    return response 

def request_handler():
    auth_header_value = request.headers.get('Authorization', None)
    auth_header_prefix = current_app.config['JWT_AUTH_HEADER_PREFIX']
    token_parameter = request.args.get('token')
    if token_parameter:
        return token_parameter
    if not auth_header_value:
        return
    parts = auth_header_value.split()
    if parts[0].lower() != auth_header_prefix.lower():
        raise JWTError('Invalid JWT header', 'Unsupported authorization type')
    elif len(parts) == 1:
        raise JWTError('Invalid JWT header', 'Token missing')
    elif len(parts) > 2:
        raise JWTError('Invalid JWT header', 'Token contains spaces')
    return parts[1]

flask_jwt._default_request_handler = request_handler

def verify_password(username, password):
    with database_lock:
        user = User.query.filter_by(username = username).first()
        if not user or not user.verify_password(password):
            return False  
    g.user=user  
    return True

def authenticate(username, password):
    if verify_password(username,password):   
        return g.user     
    else:
        abort(401)

def identity(payload):
    user_id = payload['identity']
    with database_lock:
        user= User.query.filter_by(id = user_id).first()
    g.user=user
    return user

def parse_token(req):
    token = req.headers.get('Authorization').split()[1]
    return pyjwt.decode(token, app.config['SECRET_KEY'], algorithms='HS256')

def create_token(user):
    payload = {
        'identity': user.id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + app.config['JWT_EXPIRATION_DELTA'],
        'nbf': datetime.utcnow()
    }
    token = pyjwt.encode(payload, app.config['SECRET_KEY'], algorithm='HS256')
    return token.decode('unicode_escape')

class ExceptionAwareApi(Api):
    def handle_error(self, e):
        if isinstance(e, JWTError):
            print("JWTError")
            print(e.message)
            code = 401
            data = {'status_code': code, 'description': "JWT token is expired", 'error': 'Invalid token'}
        elif isinstance(e, pyjwt.exceptions.ExpiredSignatureError):
            print("ExpiredSignatureError")
            print(e.message)
            code = 401
            data = {'status_code': code, 'description': "Signature has expired", 'error': 'Invalid token'}
        else:
            return super(ExceptionAwareApi, self).handle_error(e)            
        return self.make_response(data, code)


e = create_engine(dbConnectionString)
db = SQLAlchemy(app)

api =  ExceptionAwareApi(app)
jwt = JWT(app, authenticate, identity)


class User(db.Model):
    def __init__(self, user_type, username, wechat, qq, password, email=None, phone_number=None, deleted=0, confirmed=0):
        self.user_type = user_type
        self.username = username
        self.email = email
        self.phone_number = phone_number
        self.wechat = wechat
        self.qq = qq
        self.password = password
        self.deleted = 0 if deleted is None else deleted
        self.confirmed = 0 if confirmed is None else confirmed
        self.hash_password(self.password)

    __tablename__ = 'users'
    
    id = db.Column('id', db.Integer, primary_key=True)
    user_type = db.Column('user_type', db.Integer, index=True, nullable=False)
    username = db.Column('username', db.String(80), index=True, nullable=False, unique=True)
    email = db.Column('email', db.String(200), index=True, default=None)
    phone_number = db.Column('phone_number', db.String(11), index=True)
    wechat = db.Column('wechat', db.String(30), index=True)
    qq = db.Column('qq', db.String(20), index=True)
    password = db.Column('password', db.String(30), nullable=False)
    deleted = db.Column('deleted', db.Integer, default=0)
    confirmed = db.Column('confirmed', db.Integer, default=0)
    register_time = db.Column('register_time', db.DateTime, index=True, default=datetime.now)
    login_time = db.Column('login_time', db.DateTime, index=True, default=datetime.now)

    def hash_password(self, password):
        self.password = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

    def generate_auth_token(self, expiration = 600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    def generate_confirm_token(self):
        s = Serializer(app.config['SECRET_KEY'], expires_in=24 * 60 * 60)
        return s.dumps({ 'id': self.id })

    def confirm(self, token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('id') != str(self.id):
            return False
        self.confirmed = 1
        self.save_to_db()
        return True

    def save_to_db(self):
        with database_lock:
            db.session.add(self)
            db.session.commit()

    def delete_from_db(self):  
        with database_lock:      
            db.session.delete(self)
            db.session.commit()

    def json(self):
        return {'id': self.id, 'username': self.username}

    @classmethod
    def find(cls, cri, is_deleted):
        if is_deleted != -1:
            cri['deleted'] = is_deleted
        with database_lock:
            return cls.query.filter_by(**cri).all()

    @classmethod
    def find_by_username(cls, username, is_deleted=0):
        cri = {'username': username}
        return User.find(cri, is_deleted)

    @classmethod
    def find_by_id(cls, _id, is_deleted=0):
        cri = {'id': _id}
        return User.find(cri, is_deleted)

    @classmethod
    def find_by_email(cls, email, is_deleted = 0):
        cri = {'email': email}
        return User.find(cri, is_deleted)

    @classmethod
    def find_by_phone(cls, phone, is_deleted = 0):
        cri = {'phone_number': phone}
        return User.find(cri, is_deleted)

    @classmethod
    def is_user_confirmed(cls, _id):
        cri = {'id': _id}
        user = User.find(cri, -1)
        return user['confirmed'] == 1


class Content(db.Model):
    __tablename__ = 'contents'   

    id = db.Column('id', db.Integer, primary_key=True)
    content_type = db.Column('content_type', db.Integer, index=True, nullable=False)
    author = db.Column('author', db.String(100))
    title = db.Column('title', db.String(500))
    body = db.Column('body', db.String(100000))
    tags = db.Column('tags', db.String(100))
    description = db.Column('description', db.String(1000))
    image = db.Column('image', db.String(1000))
    image_des = db.Column('image_des', db.String(1000))
    lang = db.Column('lang', db.String(2))
    deleted = db.Column('deleted', db.Integer, default=0)
    modified_time = db.Column('modified_time', db.DateTime, index=True, default=datetime.now)
    created_time = db.Column('created_time', db.DateTime, index=True, default=datetime.now)

    def __init__(self, content_type, author, title, body, tags, lang='CN', image=None, image_des=None, description=None, deleted=0):
        self.content_type = content_type
        self.author = author
        self.title = title
        self.body = body
        self.tags = tags  
        self.image = image
        self.lang = lang
        self.image_des = image_des
        self.description = description
        
    def json(self):
        return {'id': self.id, 'content_type': self.content_type, \
        'author': self.author, 'title': self.title, \
        'body': self.body, 'tags': self.tags, 'description': self.description, \
        'image': self.image, 'image_des': self.image_des, 'lang': self.lang, \
        'modified_time': str(self.modified_time), 'created_time': str(self.created_time)}

    def save_to_db(self):
        with database_lock:
            db.session.add(self)
            db.session.commit()

    def delete_from_db(self): 
        with database_lock:       
            db.session.delete(self)
            db.session.commit()

    @classmethod
    def find(cls, cri, is_deleted):
        if is_deleted != -1:
            cri['deleted'] = is_deleted
        with database_lock:
            return cls.query.filter_by(**cri).all()

    @classmethod
    def find_by_author(cls, author, is_deleted=0):
        cri = {'author': author}
        return Content.find(cri, is_deleted)

    @classmethod
    def find_by_id(cls, _id, is_deleted=0):
        cri = {'id': _id}
        return Content.find(cri, is_deleted)

    @classmethod
    def find_by_type(cls, content_type, is_deleted=0):
        cri = {'content_type': content_type}
        return Content.find(cri, is_deleted)

    @classmethod
    def find_by_tag(cls, tag, is_deleted=0):
        with database_lock:
            if is_deleted == -1:
                return cls.query.filter(Content.tags.like('%{}%'.format(tag))).all()
            return cls.query.filter(Content.tags.like('%{}%'.format(tag)), deleted=is_deleted).all()

class Comment(db.Model):
    __tablename__ = 'comments'   

    id = db.Column('id', db.Integer, primary_key=True)
    content_id = db.Column('content_id', db.Integer, db.ForeignKey(Content.id), nullable=False)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey(User.id), nullable=False)
    cmnt = db.Column('cmnt', db.String(10000))
    deleted = db.Column('deleted', db.Integer, default=0)
    modified_time = db.Column('modified_time', db.DateTime, index=True, default=datetime.now)
    created_time = db.Column('created_time', db.DateTime, index=True, default=datetime.now)

    def __init__(self, content_id, user_id, cmnt, image=None, deleted=0):
        self.content_id = content_id
        self.user_id = user_id
        self.cmnt = cmnt
        self.image = image
    
    def json(self):
        return {'id': self.id, 'content_id': self.content_id, \
        'user_id': self.user_id, 'cmnt': self.cmnt, \
        'deleted': self.deleted, \
        'self.modified_time': str(self.modified_time), 'self.created_time': str(self.created_time)}   

    def save_to_db(self):
        with database_lock:
            db.session.add(self)
            db.session.commit()

    def delete_from_db(self):  
        with database_lock:      
            db.session.delete(self)
            db.session.commit()

    @classmethod
    def find(cls, cri, is_deleted):
        if is_deleted != -1:
            cri['deleted'] = is_deleted
        with database_lock:
            return cls.query.filter_by(**cri).all() 

    @classmethod
    def find_by_content_id(cls, content_id, is_deleted=0):
        cri = {'content_id': content_id}
        return Comment.find(cri, is_deleted)

    @classmethod
    def find_by_user_id(cls, user_id, is_deleted=0):
        cri = {'user_id': user_id}
        return Comment.find(cri, is_deleted)

    @classmethod
    def find_by_id(cls, id, is_deleted=0):
        cri = {'id': id}
        return Comment.find(cri, is_deleted)

class DownloadInfo(db.Model):
    __tablename__ = 'download_info'   

    id = db.Column('id', db.Integer, primary_key=True)
    download_content = db.Column('download_content', db.String(500), nullable=False)
    company = db.Column('company', db.String(100))
    name = db.Column('name', db.String(20), nullable=False)
    position = db.Column('position', db.String(20))
    phone = db.Column('phone', db.String(14))
    business = db.Column('business', db.String(140))
    email = db.Column('email', db.String(100), nullable=False)
    download_time = db.Column('download_time', db.DateTime, default=datetime.now)

    def __init__(self, download_content, email, name, company=None, position=None, business=None, phone=None):
        self.download_content = download_content
        self.email = email
        self.company = company
        self.name = name
        self.position = position
        self.business = business
        self.phone = phone
        self.download_time = datetime.now()
    
    def json(self):
        return {'id': self.id, 'download_content': self.download_content, 'download_time': self.download_time, 'email': self.email}   

    def save_to_db(self):
        with database_lock:
            db.session.add(self)
            db.session.commit()

    def delete_from_db(self):
        with database_lock:        
            db.session.delete(self)
            db.session.commit()

    @classmethod
    def find(cls, cri):
        with database_lock:
            return cls.query.filter_by(**cri).all() 


def get_user_by_different_status(id_, is_deleted):
    try:
        if id_:
            users = User.find_by_id(id_, is_deleted)
        else:
            users = User.find({}, is_deleted)
        if len(users) == 0:
            return {'error': 'did not find any user.'}, 404
        return {'user': [user.json() for user in users]}, 200
    except Exception as e:
        return {'error': str(e)}, 400

def get_content_by_different_criterion(criterion, value, is_deleted):
    try:
        if criterion == 'content_type':
            contents = Content.find_by_type(value, is_deleted)
        elif criterion == 'author':
            contents = Content.find_by_author(value, is_deleted)
        elif criterion == 'tags':
            contents = Content.find_by_tag(value, is_deleted)
        elif criterion == 'id':
            if value:
                contents = Content.find_by_id(value, is_deleted)
            else:
                contents = Content.find({}, is_deleted)
        else:
            raise Exception('wrong searching parameter')
        if len(contents) == 0:
            return {'error': 'did not find any content'}, 404
        return {'contents': [cnt.json() for cnt in contents]}
    except Exception as e:
        return {'error': str(e)}, 400

def get_comment_by_different_criterion(criterion, value, is_deleted):
    try:
        if criterion == 'content_id':
            comments = Comment.find_by_content_id(value, is_deleted)
        elif criterion == 'user_id':
            comments = Comment.find_by_user_id(value, is_deleted)
        elif criterion == 'id':
            if value:
                comments = Comment.find_by_id(value, is_deleted)
            else:
                comments = Comment.find({}, is_deleted)
        else:
            raise Exception('wrong searching parameter')
        if len(comments) == 0:
            return {'error': 'did not find any content'}, 404
        return {'comments': [cmt.json() for cmt in comments]}
    except Exception as e:
        return {'error': str(e)}, 400

class UserAll(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_user_by_different_status(id_, -1)

class UserActive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_user_by_different_status(id_, 0)

class UserDeactive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_user_by_different_status(id_, 1)

class CreateUser(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('user_type', type=int, required=False)
    parser.add_argument('username', type=str, required=False)
    parser.add_argument('email', type=str, required=False)
    parser.add_argument('phone_number', type=str, required=False)
    parser.add_argument('wechat', type=str, required=False)
    parser.add_argument('qq', type=str, required=False)
    parser.add_argument('password', type=str, required=False)
    parser.add_argument('deleted', type=int, required=False)
    parser.add_argument('confirmed', type=int, required=False)
    
    def post(self, type_):
        try:
            data = UserExecute.parser.parse_args()

            if User.find_by_username(data['username'], 0):
                return {'error': 'username is already is use.'}, 400
            if not data['phone_number'] == '':
                if User.find_by_phone(data['phone_number'], 0):
                    return {'error': 'phone number is already in use.'}, 400
            else:
                data.pop('phone_number', None)
            if not data['email'] == '':
                if User.find_by_email(data['email'], 0):
                    return {'error': 'email is already in use.'}, 400
            else:
                data.pop('email', None)

            if type_ == 'phone':
                found_in_deactive = User.find_by_phone(data['phone_number'], 1)
                if found_in_deactive:
                    for f in found_in_deactive:
                        f.delete_from_db()

                if data['user_type'] is None or data['phone_number'] is None or data['password'] is None:
                    raise Exception('user_type, phone_number, password cannot be empty')
            elif type_ == 'email':
                found_in_deactive = User.find_by_email(data['email'], 1)
                if found_in_deactive:
                    for f in found_in_deactive:
                        f.delete_from_db()

                if data['user_type'] is None or data['email'] is None or data['password'] is None:
                    raise Exception('user_type, email, password cannot be empty')

            user = User(**data)
            if type_ == 'email':
                user.confirmed = 0
            else:
                user.confirmed = 1
            user.save_to_db()
            return {'message': str(user.id)}, 201
        except Exception as e:
            return {'error': str(e)}, 400


class UserExecute(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('user_type', type=int, required=False)
    parser.add_argument('username', type=str, required=False)
    parser.add_argument('email', type=str, required=False)
    parser.add_argument('phone_number', type=str, required=False)
    parser.add_argument('wechat', type=str, required=False)
    parser.add_argument('qq', type=str, required=False)
    parser.add_argument('password', type=str, required=False)
    parser.add_argument('deleted', type=int, required=False)
    parser.add_argument('confirmed', type=int, required=False)

    @jwt_required()
    def put(self, id_):
        try:
            data = UserExecute.parser.parse_args()
            if id_ is None:
                return {'error': 'PUT has to be executed with an id'}, 400
            user = User.find_by_id(id_, -1)
            if data['username'] is not None:
                return {'error': 'username is not changable'}, 400
            if len(user) == 0:
                return {'error': 'no user found'}, 404
            user = user[0]

            if data['user_type'] is not None:
                user.user_type = data['user_type']
            if data['email'] is not None:
                user.email = data['email']
            if data['phone_number'] is not None:
                user.phone_number = data['phone_number']
            if data['wechat'] is not None:
                user.wechat = data['wechat']
            if data['qq'] is not None:
                user.qq = data['qq']
            if data['password'] is not None:
                user.password = generate_password_hash(data['password'])
            if data['deleted'] is not None:
                user.deleted = data['deleted']
            if data['confirmed'] is not None:
                user.confirmed = data['confirmed']

            user.save_to_db()
            return {'message': 'user is updated.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def delete(self, id_):
        try:
            if id_ is None:
                return {'error': 'DELETE has to be executed with an id'}, 400
            user = User.find_by_id(id_, -1)
            if len(user) == 0:
                return {'error': 'no user found'}, 404
            user = user[0]
            user.deleted = 1
            user.save_to_db()
            return {'message': 'user is deleted.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

class ContentAll(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('id', id_, -1)
        
class ContentTypeActive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('content_type', id_, 0)
        
class ContentAuthorActive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('author', id_, 0)
        
class ContentTagsActive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('tags', id_, 0)
        
class ContentTypeDeactive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('content_type', id_, 1)
        
class ContentAuthorDeactive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('author', id_, 1)
        
class ContentTagsDeactive(Resource):
    def get(self, id_=None):
        return get_content_by_different_criterion('tags', id_, 1)

class ContentExecute(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('content_type', type=int, required=False)
    parser.add_argument('author', type=str, required=False)
    parser.add_argument('title', type=str, required=False)
    parser.add_argument('body', type=str, required=False)
    parser.add_argument('tags', type=str, required=False)
    parser.add_argument('description', type=str, required=False)
    parser.add_argument('image', type=str, required=False)
    parser.add_argument('image_des', type=str, required=False)
    parser.add_argument('lang', type=str, required=False)
    parser.add_argument('deleted', type=int, required=False)

    @jwt_required()
    def post(self):
        try:
            data = ContentExecute.parser.parse_args()
            if 'content_type' not in data.keys():
                raise Exception('content_type cannot be empty')
            content = Content(**data)
            content.save_to_db()

            return {'message': 'content has been created successfully.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def put(self, id_):
        try:
            data = ContentExecute.parser.parse_args()
            if id_ is None:
                return {'error': 'PUT has to be executed with an id'}, 400
            content = Content.find_by_id(id_, -1)
            if len(content) == 0:
                return {'error': 'no content found'}, 404
            content = content[0]

            if data['content_type'] is not None:
                content.content_type = data['content_type']
            if data['author'] is not None:
                content.author = data['author']
            if data['title'] is not None:
                content.title = data['title']
            if data['body'] is not None:
                content.body = data['body']
            if data['tags'] is not None:
                content.tags = data['tags']
            if data['description'] is not None:
                content.description = data['description']
            if data['image'] is not None:
                content.image = data['image']
            if data['image_des'] is not None:
                content.image_des = data['image_des']
            if data['lang'] is not None:
                content.lang = data['lang']
            if data['deleted'] is not None:
                content.deleted = data['deleted']
            if data['lang'] is not None:
                content.lang = data['lang']
            content.modified_time = datetime.now()

            content.save_to_db()
            return {'message': 'content is updated.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def delete(self, id_):
        try:
            if id_ is None:
                return {'error': 'DELETE has to be executed with an id'}, 400
            content = Content.find_by_id(id_, -1)
            if len(content) == 0:
                return {'error': 'no content found'}, 404
            content = content[0]
            content.deleted = 1
            content.save_to_db()
            return {'message': 'content is deleted.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

class CommentAll(Resource):
    def get(self, id_=None):
        return get_comment_by_different_criterion('id', id_, -1)

class CommentContentTypeActive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('content_id', id_, 0)

class CommentUserIdActive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('user_id', id_, 0)

class CommentIdActive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('id', id_, 0)

class CommentContentTypeDeactive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('content_type', id_, 1)

class CommentUserIdDeactive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('user_id', id_, 1)

class CommentIdDeactive(Resource):
    @jwt_required()
    def get(self, id_=None):
        return get_comment_by_different_criterion('id', id_, 1)

class CommentExecute(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('content_id', type=int, required=False)
    parser.add_argument('user_id', type=int, required=False)
    parser.add_argument('cmnt', type=str, required=False)
    parser.add_argument('deleted', type=int, required=False)
    @jwt_required()
    def post(self):
        try:
            data = CommentExecute.parser.parse_args()

            if data['content_id'] is None or data['user_id'] is None:
                raise Exception('content_id, user_id cannot be empty')

            comment = Comment(**data)
            comment.save_to_db()

            return {'message': 'comment has been created successfully.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def put(self, id_):
        try:
            data = CommentExecute.parser.parse_args()
            if id_ is None:
                return {'error': 'PUT has to be executed with an id'}, 400
            comment = Comment.find_by_id(id_, -1)
            if len(comment) == 0:
                return {'error': 'no comment found'}, 404
            comment = comment[0]

            if data['content_id'] is not None:
                comment.content_id = data['content_id']
            if data['user_id'] is not None:
                comment.user_id = data['user_id']
            if data['cmnt'] is not None:
                comment.cmnt = data['cmnt']
            if data['deleted'] is not None:
                comment.deleted = data['deleted']
            comment.modified_time = datetime.now()
            comment.save_to_db()
            return {'message': 'comment is updated.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def delete(self, id_):
        try:
            if id_ is None:
                return {'error': 'DELETE has to be executed with an id'}, 400
            comment = Comment.find_by_id(id_, -1)
            if len(comment) == 0:
                return {'error': 'no comment found'}, 404
            comment = comment[0]
            comment.deleted = 1
            comment.save_to_db()
            return {'message': 'comment is deleted.'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

class LoginWork(Resource):
    def post(self):
        data = request.form
        username = data.get('username')
        password = data.get('password')
        
        if not username or not password:
            return {'error': 'username or password can not be empty.'}, 400
        else:
            u = authenticate(username, password)
            return {'token': create_token(u), 'user': u.json()}, 201

class ValidateToken(Resource):
    @jwt_required()
    def get(self):
       return {'token_valid':'true' }

class RefreshToken(Resource):
   @jwt_required()
   def get(self):
       token = request.headers.get('Authorization').split()[1]
       newToken=create_token(g.user)
       return {'new_token':newToken }

def generate_random_code():
    ver = []
    for i in range(6):
        ver.append(str(random.randint(0, 9)))
    code = ''.join(ver)
    return code 

class SMSVerify(Resource):
    def post(self):
        try:
            data = request.form
            api_key = 'c771a5bb232ce728f182b77686632403'
            mobile = data.get('mobile')
            if mobile == '':
                raise Exception('phone number cannot be empty')
            else:
                if User.find_by_phone(mobile):
                    raise Exception('phone number is already in use.')
            code = generate_random_code()
            text = '【心鉴智控】您的验证码是{}'.format(code)
            send_data = {
                'apikey': api_key,
                'mobile': mobile,
                'text': text
            }
            headers = {'Accept': 'application/json;charset=utf-8;', 'Content-Type': 'application/x-www-form-urlencoded'}
            resp = requests.post('https://sms.yunpian.com/v2/sms/single_send.json', data=send_data, headers=headers)
            res = resp.json()
            if res['code'] != 0:
                raise Exception('error code: {}, {}'.format(res['msg'], res['detail']))
            return {'message': 'verification code sent. {}'.format(base64.b64encode(code.encode()).decode("utf-8"))}, 200
        except Exception as e:
            return {'error': '{}'.format(str(e))}, 400

class DownloadAttach(Resource):
    def post(self):
        try:
            data = request.form
            download_content = data.get('download_content')
            company = data.get('company')
            name = data.get('name')
            position = data.get('position')
            phone = data.get('phone')
            business = data.get('business')
            email = data.get('email')

            if email is None or name is None:
                raise Exception('Name or Email couldnot be empty')

            body = u'尊敬的{}{}：\n您请求下载的资料：“{}”已发至您的邮箱。请查看附件。\n谢谢！\n心鉴科技'.format(company, name, download_content)
            text = MIMEText(body)
            msg = MIMEMultipart()
            subject = u'心鉴科技资料-{}'.format(download_content)
            msg['From'] = 'engineering@seeking.ai'
            msg['To'] = email
            msg['Subject'] = subject
            msg.attach(text)

            part = MIMEApplication(open(os.path.join('resources', '{}.pdf'.format(download_content)), 'rb').read())            
            part.add_header('Content-Disposition', 'attachment', filename='{}.pdf'.format(download_content))            
            msg.attach(part)

            s = smtplib.SMTP('smtp.seeking.ai')
            s.login('engineering@seeking.ai', 'xjzk654321')
            s.send_message(msg)
            s.quit()

            download = DownloadInfo(download_content=download_content, company=company, name=name, position=position, phone=phone, business=business, email=email)
            download.save_to_db()

            return {'message': 'sent email'}, 201
        except Exception as e:
            return {'error': str(e)}, 400
            

class SendEmail(Resource):
    def post(self, id_):
        try:
            data = request.form
            email = data.get('email')
            if email is None:
                raise Exception('no email found from the request.')
            s = Serializer(app.config['SECRET_KEY'], expires_in=24 * 60 * 60)
            link_dumps = s.dumps({'id': id_}).decode('utf-8')
            body = u'您注册的账号为{}。验证链接为: https://seeking.ai/#/email?token={} \n验证码将在24小时后过期，请登录seeking.ai填写验证码，完成注册。'.format(email, link_dumps)
            msg = MIMEText(body)
            subject = u'[DO NOT REPLY]Seeking.ai 验证码'
            msg['From'] = "engineering@seeking.ai"
            msg['To'] = email
            msg['Subject'] = subject

            s = smtplib.SMTP('smtp.seeking.ai')
            s.login('engineering@seeking.ai','xjzk654321')
            s.send_message(msg)
            s.quit()
            return {'message': 'sent email.'}, 201
        except Exception as e:
            return {'error': 'sending email wrong. {}'.format(str(e))}, 400

class Active(Resource):
    @jwt_required()
    def get(self, token):
        try:
            id_ = current_identity.id
            user = User.find_by_id(id_)[0]
            if user is None:
                raise Exception('no user found')
            if user.confirmed == 1:
                return {'message': 'user already activated.'}, 200
            user.confirm(token)
            return {'message': 'successful activated.'}, 200
        except Exception as e:
            return {'error': str(e)}, 400
            

class UploadImage(Resource):
    @jwt_required()
    def post(self):
        try:
            images = request.files.getlist('files')
            user = current_identity.username
            proj = request.form.get('project')
            test_or_train = request.form.get('test_or_train')
            good_or_bad = request.form.get('good_or_bad')

            if proj is None or test_or_train is None or good_or_bad is None:
                raise Exception('user, project, test_or_train, good_or_bad cannot be null')

            path = os.path.join(r'c:\automl', user, proj, 'images', test_or_train, good_or_bad)
            if not os.path.exists(path):
                os.makedirs(path)
            image_names = []
            for image in images:
                image_names.append(image.filename)
                with open(path + '\\' + image.filename, 'wb') as f:
                    image.save(f)
            return {'message': image_names}, 201
        except Exception as e:
            return {'error': str(e)}, 400


processes = {}


class Training(Resource):        
    @jwt_required()
    def get(self):
        try:
            data = request.form
            user = current_identity.username
            proj = data['project']
            with open(r'c:\automl\stats.json') as j:
                stats_json = load(j)
                for keyvalues in stats_json.items():
                    if keyvalues[1]['current_user'] == user and keyvalues[1]['current_project'] == proj:
                        project_status_path = os.path.join(r'c:\automl', user, proj, 'result', 'result.json')
                        with open(project_status_path, 'rb') as f:
                            current_status = load(f)
                            if current_status['progress'] == 1:
                                return {'message': 'project training finished.'}, 200   #项目训练结束
                            return {'progress': current_status['progress']}, 200        #返回训练进度                    
            return {'message': 'no projects are training now.'}, 200    #没有在训练的项目
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def post(self):
        try:
            global processes
            data = request.form
            user = current_identity.username
            proj = data['project']
            net_type = data['net_type']
            width = data['width']
            height = data['height']
            enhancement = data['enhancement'] if data['enhancement'] != '' else None

            path = os.path.join(r'c:\automl', user, proj)

            with open(r'c:\automl\stats.json', 'rb') as f:
                stats = load(f)
            gpu = -1
            for keyvalues in stats.items():
                if keyvalues[1]['current_user'] == user and keyvalues[1]['current_project'] == proj and keyvalues[1]['in_training'] == 1:
                    raise Exception('project {} is already in training. Let it finish first.'.format(proj))     #项目已经在训练，请等待完成。
                if keyvalues[1]['in_training'] == '0':
                    gpu = keyvalues[0]
                    break
            if gpu == -1:
                raise Exception('all instances are in training, please come back and start the training later.')    #所有实例都在进行训练，请晚点再来。

            current_instance = stats[gpu]

            current_instance['in_training'] = 1
            current_instance['current_user'] = user
            current_instance['current_project'] = proj
            current_instance['net_type'] = net_type
            current_instance['width'] = width
            current_instance['height'] = height
            current_instance['enhancement'] = enhancement

            stats[gpu] = current_instance
            with database_lock:
                f1 = open(r'c:\automl\stats.json', 'w')
                f1.write(dumps(stats))

            command = r'python.exe C:\Users\seeking\Documents\autolearningWebCode\main.py -gpu {}'.format(gpu[3:])
            process = subprocess.Popen(command, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)

            processes[gpu] = process.pid

            net_types = net_type.replace(' ', '').split(',')

            result_path = os.path.join('c:\automl', user, proj, 'result')
            if os.path.exists(result_path):
                shutil.rmtree(result_path)
                os.mkdir(result_path)
            
            json_path = os.path.join(result_path, 'result.json')
            into_json = {}
            into_json['progress'] = 0
            into_json['error'] = -1
            for nt in net_types:
                into_json[nt] = {}

            with open(json_path, 'w') as f:
                f.write(dumps(into_json))

            return {'message': 'successfully started training'}, 201
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def put(self):
        try:
            global processes
            user = current_identity.username
            proj = request.form['proj']
            gpu = -1
            with open(r'c:\automl\stats.json', 'rb') as f:
                origin = load(f)
            for keyvalues in origin.items():
                if keyvalues[1]['current_user'] == user and keyvalues[1]['current_project'] == proj:
                    gpu = keyvalues[0]
                    
            if processes is not None:
                if gpu == -1:
                    raise Exception('No process found')
                subprocess.check_output("Taskkill /PID {} /F".format(processes[gpu]))
                    

            stats = {}
            stats['in_training'] = 0
            stats['current_user'] = -1
            stats['current_project'] = -1
            stats['net_type'] = -1
            stats['width'] = -1
            stats['height'] = -1
            stats['enhancement'] = -1
            origin[gpu] = stats
            with database_lock:
                f1 = open(r'c:\automl\stats.json', 'w')
                f1.write(dumps(origin))
            return {'message': 'successfully stopped training'}, 201
        except Exception as e:
            gpu = -1
            with open(r'c:\automl\stats.json', 'rb') as f:
                origin = load(f)
            for keyvalues in origin.items():
                if keyvalues[1]['current_user'] == user and keyvalues[1]['current_project'] == proj:
                    gpu = keyvalues[0]
            stats = {}
            stats['in_training'] = 0
            stats['current_user'] = -1
            stats['current_project'] = -1
            stats['net_type'] = -1
            stats['width'] = -1
            stats['height'] = -1
            stats['enhancement'] = -1
            origin[gpu] = stats
            with database_lock:
                f1 = open(r'c:\automl\stats.json', 'w')
                f1.write(dumps(origin))
            if e.returncode == 128:
                return {'message': 'process is already terminated.'}, 201
            return {'error': str(e)}, 400

class TrainingResult(Resource):
    @jwt_required()
    def get(self, proj):
        try:
            user = current_identity.username

            result_path = os.path.join(r'c:\automl', user, proj, 'result')
            if not os.path.exists(result_path):
                raise Exception('current user has no training result or current user has no project named: {}'.format(proj))

            result_json_path = os.path.join(result_path, 'result.json')
            with open(result_json_path, 'rb') as f:
                result = load(f)
                if not result['error'] == '-1':
                    raise Exception(result['error'])
                result.pop('error', None)
                result.pop('progress', None)
                data = {}
                data['json'] = result
                for net_type in result.keys():
                    if 'path' not in data.keys():
                        data['path'] = ['{}.pb'.format(net_type)]
                    else:
                        data['path'].append('{}.pb'.format(net_type))
            return {'message': dumps(data)}, 200
        except Exception as e:
            return {'error': str(e)}, 400
    
    @jwt_required()
    def delete(self, proj):
        try:
            user = current_identity.username
            user_path = os.path.join(r'c:\automl', user)
            proj_path = os.path.join(r'c:\automl', user, proj)
            if not os.path.exists(proj_path):
                raise Exception('current user has no project named: {}. deleting project failed.'.format(proj))
            shutil.rmtree(proj_path)

            if len(os.listdir(user_path)) == 0:
                shutil.rmtree(user_path)

            return {'message': 'successfully removed project'}, 201
        except Exception as e:
            return {'error': str(e)}, 400


class LoadTrainingPage(Resource):  
    @jwt_required()
    def get(self):
        try:
            user = current_identity.username
            user_path = os.path.join(r'c:\automl', user)
            if os.path.exists(user_path):
                projects = os.listdir(user_path)
                if len(projects) == 0:
                    return {'message': 'current user has no trained or training model.'}, 200
                finished = []
                unfinished = []
                ongoing = []
                for d in projects:
                    result_path = os.path.join(user_path, d, 'result', 'result.json')
                    if os.path.isfile(result_path):
                        with open(result_path, 'rb') as f:
                            project_status = load(f)
                            if 'progress' in project_status.keys():
                                progress = project_status['progress']
                                if progress == 1.0:
                                    finished.append(d)
                                else:
                                    ongoing.append(d)
                            else:
                                raise Exception('error in loading result.json file')
                    else:
                        unfinished.append(d)
                res = {'finished': finished, 'unfinished': unfinished, 'ongoing': ongoing}
                return {'message': dumps(res)}, 200
            else:
                return {'message': 'current user has no trained or training model.'}, 200
        except Exception as e:
            return {'error': str(e)}, 400

class GetModel(Resource):
    @jwt_required()
    def get(self, proj, name):
        try:
            dir = os.path.join(r'c:/automl/', current_identity.username, proj, 'result')
            abs_path = os.path.join(dir, name + '.pb')
            if os.path.isfile(abs_path):
                with open(abs_path, 'rb') as f:
                    pb = f.read()
            if pb is not None:
                # resp = make_response(pb)
                # return resp
                return send_from_directory(dir, name + '.pb', as_attachment=True)
        except Exception as e:
            return {'error': str(e)}, 400

class ProjectStatus(Resource):
    @jwt_required()
    def get(self, proj):
        try:
            user = current_identity.username
            proj_path = os.path.join(r'c:\automl', user, proj)
            image_path = os.path.join(proj_path, 'images')
            if not os.path.exists(image_path):
                raise Exception('no images uploaded.')              #没有图片上传。
            if not os.path.exists(os.path.join(image_path, 'test')):
                raise Exception('test images are not uploaded.')    #测试图片未完成上传。
            if not os.path.exists(os.path.join(image_path, 'test', 'good')):
                raise Exception('test GOOD images are not uploaded.')    #测试图片（好）未完成上传。
            if not os.path.exists(os.path.join(image_path, 'test', 'bad')):
                raise Exception('test BAD images are not uploaded.')    #测试图片（坏）未完成上传。
            if not os.path.exists(os.path.join(image_path, 'train')):
                raise Exception('train images are not uploaded.')   #训练图片未完成上传。
            if not os.path.exists(os.path.join(image_path, 'train', 'good')):
                raise Exception('train GOOD images are not uploaded.')    #训练图片（好）未完成上传。
            if not os.path.exists(os.path.join(image_path, 'train', 'bad')):
                raise Exception('train BAD images are not uploaded.')    #训练图片（坏）未完成上传。
            status = {}
            if os.path.isfile(os.path.join(proj_path, 'status.json')):
                with open(os.path.join(proj_path, 'status.json'), 'rb') as f:
                    status = load(f)
            return {'status': status}
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def post(self, proj):
        try:
            user = current_identity.username
            status_path = os.path.join(r'c:\automl', user, proj, 'status.json')
            status = request.form
            with open(status_path, 'w') as f:
                f.write(dumps(status))
            return  {'message': 'successfully saved project status.'}       #保存训练项目参数成功
        except Exception as e:
            return {'error': str(e)}, 400

    @jwt_required()
    def put(self, proj):
        try:
            user = current_identity.username
            status_path = os.path.join(r'c:\automl', user, proj, 'status.json')
            status = request.form
            with open(status_path, 'rb') as f:
                original_status = load(f)
            for key_value in status.items():
                original_status[key_value[0]] = key_value[1]
            with open(status_path, 'w') as f:
                f.write(dumps(original_status))
            return  {'message': 'successfully saved project status.'}       #更新训练项目参数成功
        except Exception as e:
            return {'error': str(e)}, 400


api.add_resource(RefreshToken, '/tokenrefresh/', strict_slashes=False)

api.add_resource(UserAll, '/users/', '/users/<id_>', strict_slashes=False)
api.add_resource(UserActive, '/users/active', strict_slashes=False)
api.add_resource(UserDeactive, '/users/deactive', strict_slashes=False)
api.add_resource(UserExecute, '/user/', '/user/<id_>', strict_slashes=False)
api.add_resource(CreateUser, '/create/<type_>/', strict_slashes=False)

api.add_resource(ContentAll, '/contents/', '/contents/<id_>', strict_slashes=False)
api.add_resource(ContentTypeActive, '/contents/active/type', '/contents/active/type/<id_>', strict_slashes=False)
api.add_resource(ContentAuthorActive, '/contents/active/author', '/contents/active/author/<id_>', strict_slashes=False)
api.add_resource(ContentTagsActive, '/contents/active/tags', '/contents/active/tags/<id_>', strict_slashes=False)
api.add_resource(ContentTypeDeactive, '/contents/deactive/type', '/contents/deactive/type/<id_>', strict_slashes=False)
api.add_resource(ContentAuthorDeactive, '/contents/deactive/author', '/contents/deactive/author/<id_>', strict_slashes=False)
api.add_resource(ContentTagsDeactive, '/contents/deactive/tags', '/contents/deactive/tags/<id_>', strict_slashes=False)
api.add_resource(ContentExecute, '/content/', '/content/<id_>', strict_slashes=False)

api.add_resource(CommentAll, '/comments/', '/comments/<id_>', strict_slashes=False)
api.add_resource(CommentContentTypeActive, '/comments/active/content/', '/comments/active/content/<id_>', strict_slashes=False)
api.add_resource(CommentUserIdActive, '/comments/active/userid/', '/comments/active/userid/<id_>', strict_slashes=False)
api.add_resource(CommentIdActive, '/comments/active/id/', '/comments/active/id/<id_>', strict_slashes=False)
api.add_resource(CommentContentTypeDeactive, '/comments/deactive/content/', '/comments/deactive/content/<id_>', strict_slashes=False)
api.add_resource(CommentUserIdDeactive, '/comments/deactive/userid/', '/comments/deactive/userid/<id_>', strict_slashes=False)
api.add_resource(CommentIdDeactive, '/comments/deactive/id/', '/comments/deactive/id/<id_>', strict_slashes=False)
api.add_resource(CommentExecute, '/comment/', '/comment/<id_>', strict_slashes=False)

api.add_resource(DownloadAttach, '/download/', strict_slashes=False)

api.add_resource(LoginWork, '/login/', strict_slashes=False)

api.add_resource(SMSVerify, '/sms_verify/', strict_slashes=False)
api.add_resource(SendEmail, '/send_email/<id_>', strict_slashes=False)
api.add_resource(Active, '/active/<token>', strict_slashes=False)
api.add_resource(UploadImage, '/upload_imgs/', strict_slashes=False)
api.add_resource(Training, '/training/', strict_slashes=False)
api.add_resource(TrainingResult, '/training_result/<proj>/', strict_slashes=False)
api.add_resource(LoadTrainingPage, '/load_training_page/', strict_slashes=False)
api.add_resource(GetModel, '/get_model/<proj>/<name>', strict_slashes=False)
api.add_resource(ProjectStatus, '/load_project/<proj>/', strict_slashes=False)


if __name__ == '__main__': 
    # init_db()
    try:
        # app.run()
        app.run(ssl_context=(r'c:\certs\apiserver_crt.pem', r'c:\certs\apiserver_key.pem'))
    except:
        pass